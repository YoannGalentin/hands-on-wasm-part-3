package main

import (
	"syscall/js"
)

func Handle(this js.Value, args []js.Value) interface{} {
	// get the first parameter
	human := args[0]
	// get members of human
	firstName := human.Get("firstname").String()
	lastName := human.Get("lastname").String()

	return map[string]interface{} {
		"text": "Hey " + firstName + " " + lastName,
	}
}

func main() {

	js.Global().Set("Handle",js.FuncOf(Handle))

	<-make(chan bool)
}
