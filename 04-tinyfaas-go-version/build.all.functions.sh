#!/bin/bash

function build_wasm {
  function_name=$1
  function_code_path=$2
  cd ${function_code_path}
  GOOS=js GOARCH=wasm go build \
  -o ../../wasm/${function_name}.wasm
  cd ../..
}

build_wasm hello ./functions/hello
build_wasm hey ./functions/hey

ls -lh ./wasm/*.wasm
