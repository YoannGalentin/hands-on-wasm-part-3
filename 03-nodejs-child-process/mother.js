
const fork = require('child_process').fork
childProcess01 = fork("./child-process.js")
childProcess02 = fork("./child-process.js")

childProcess01.send({
  text: "🤖> Hello from Mum"
})

childProcess02.send({
  text: "🤖> Hello from Mum"
})


childProcess01.on("message", (message) => {
  console.log("🤖👋> Received by Mum", message)
})

childProcess02.on("message", (message) => {
  console.log("🤖👋> Received by Mum", message)
})
