# Tiny FaaS (Rust version)

Do the same Tiny FaaS but with Rust and Node.js

- Build the 2 wasm functions
- Look at the bottom file of `./functions/hola/hola.js`
- Look at the bottom file of `./functions/salut/salut.js`
- Copy `./functions/hola/hola.js` and `./functions/hola/hola_bg.wasm` to `./wasm`
- Copy `./functions/salut/salut.js` and `./functions/salut/salut_bh.wasm` to `./wasm`
- Modify `index.js` and `child.js` from the previous project (Go version)
